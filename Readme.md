

####  一   说明

​	frp 版本为  0.43.0 ，其中服务端部署在 linux 上， 客户端部署在 windows 上。其他版本去 https://hub.fastgit.xyz/fatedier/frp/releases  下载



#### 二 部署操作

##### 2.1  服务端部署

```bash
1. 上传 linux 服务器
2. tar -zxvf frp_0.43.0_linux_amd64.tar.gz
3. cd frp_0.43.0_linux_amd64/
4. 将 frps.ini 上传或 直接 vi frps.ini  进行修改
5. 将frps.service上传到解压目录下， 设置和启动 fpr
sudo mkdir -p /etc/frp
sudo cp frps.ini /etc/frp
sudo cp frps /usr/bin
sudo cp frps.service /etc/systemd/system/
sudo systemctl enable frps
sudo systemctl start frps
systemctl status frps
6.访问 ip:7500 可以看到 fpr 管理界面
```

##### 	2.2 客户端部署

```
1.解压 frp_0.43.0_windows_amd64.zip
2.修改或直接替换 frpc.ini 配置文件中内容
3.点击 start.bat 启动， 看到 start proxy success即启动成功
```

```
 cp frpc.ini /etc/frp
 cp frpc /usr/bin
 cp frpc.service /etc/systemd/system/
 systemctl enable frpc
 systemctl start frpc
 systemctl status frpc
```








